package ru.blog.csssr;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class MainPage {
    WebDriver driver;
    By label = By.xpath("//ul/li/label");
    By checkbox = By.xpath("//ul/li/input");

    public MainPage(WebDriver driver){
        this.driver = driver;
    }

    public void checkCheckboxes(){
        List<WebElement> infoblocksNames = driver.findElements(By.xpath("//div[contains(@class, 'graphs-')]/a"));
        List<WebElement> infoblocks = driver.findElements(By.xpath("//div[contains(@class, 'info-')]"));
        for(int i = 0; i < infoblocksNames.size() - 1; i++){
            if(i>0) {
                infoblocksNames.get(i).click(); } //сделано чтобы не нажать на уже показанную первую вкладку и не напороться на баг
            System.out.println(infoblocksNames.get(i).getText());
            checkCheckboxesInPage(infoblocks.get(i));
        }
    }

    public void checkCheckboxesInPage(WebElement infoblock){
            List<WebElement> checkboxes = infoblock.findElements(By.xpath("./aside/ul/li"));
            for(WebElement checky : checkboxes) {
                WebElement checkboxElement = checky.findElement(By.xpath("./input"));
                WebElement labelElement = checky.findElement(By.xpath("./label"));
                System.out.print(labelElement.getText());
                labelElement.click();
                assert !(checkboxElement.isSelected());
                labelElement.click();
                assert checkboxElement.isSelected();
                System.out.println(" - работает нормально!");
            }
    }
}
