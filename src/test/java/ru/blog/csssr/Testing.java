package ru.blog.csssr;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Testing {
    public static WebDriver driver;
    public MainPage page;


    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("http://blog.csssr.ru/qa-engineer/");

    }

    @Test
    public void testing(){
        page = new MainPage(driver);
        page.checkCheckboxes();
    }


    @AfterClass
    public static void tearDown() {

        driver.quit();
    }
}
